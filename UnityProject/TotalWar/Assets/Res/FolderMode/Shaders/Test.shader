﻿Shader "Unlit/Test"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
        _LightPos ("LightPos", vector) = (0, 0, 0, 0)
        _Light ("Light", float) = 1
        _BackLight("BackLight",color) = (1,1,1,1)
        _BackLightRange("BackLightRange",range(0,1)) = 0
        _BackLightIntensity("BackLightIntensity",float) = 1
        _ForwardLight("ForwardLight",color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }

        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct v2f
            {
                float2 uv: TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex: SV_POSITION;
                float3 normal: NORMAL0;
                float4 localVertex: TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _LightPos;
            float _Light;
            fixed4 _BackLight,_ForwardLight;
            float _BackLightRange;
            float _BackLightIntensity;

            v2f vert(appdata_base v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.normal = v.normal;
                o.localVertex = v.vertex;
                UNITY_TRANSFER_FOG(o, o.vertex);
                return o;
            }

            fixed4 frag(v2f i): SV_Target
            {
                float3 worldNormal = normalize(UnityObjectToWorldNormal(i.normal));
                float3 worldLight = normalize(UnityObjectToWorldDir(normalize(_LightPos - i.localVertex)));
                float dotLN = saturate(dot(worldNormal, worldLight) * 0.5 + 0.7);

                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv) * dotLN * _Light * _ForwardLight;

                if(dotLN <= _BackLightRange)
                {
                    col = lerp(col,saturate(col * _BackLight * _BackLightIntensity),dotLN/_BackLightRange);
                }

                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }

            ENDCG
            
        }
        
        UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    }
}
