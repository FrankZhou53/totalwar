using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDLevelConfig
    {
        public Dictionary<int, float> dicWeaponInfo = new Dictionary<int, float>();
        public void Reset()
        {
            ExtractWeaponInfo();
        }
        void ExtractWeaponInfo()
        {
            if (!string.IsNullOrEmpty(weaponInfo))
            {
                var weapon = Helper.String2ListString(weaponInfo, "|");
                foreach (var item in weapon)
                {
                    var info = Helper.String2ListFloat(item, "=");
                    if (info.Count > 1)
                    {
                        dicWeaponInfo.Add((int)info[0], info[1]);
                    }
                }
            }
        }
    }
}