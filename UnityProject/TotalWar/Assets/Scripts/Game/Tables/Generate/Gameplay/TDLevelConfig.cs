//Auto Generate Don't Edit it
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDLevelConfig
    {
        
       
        private EInt m_ID = 0;   
        private EInt m_SceneID = 0;   
        private EInt m_EnemyID = 0;   
        private bool m_IsBoss = false;   
        private string m_WeaponInfo;   
        private EInt m_EnemyCount = 0;  
        
        //private Dictionary<string, TDUniversally.FieldData> m_DataCacheNoGenerate = new Dictionary<string, TDUniversally.FieldData>();
      
        /// <summary>
        /// 关卡id
        /// </summary>
        public  int  iD {get { return m_ID; } }
       
        /// <summary>
        /// 场景id
        /// </summary>
        public  int  sceneID {get { return m_SceneID; } }
       
        /// <summary>
        /// 敌人id
        /// </summary>
        public  int  enemyID {get { return m_EnemyID; } }
       
        /// <summary>
        /// 是否有boss
        /// </summary>
        public  bool  isBoss {get { return m_IsBoss; } }
       
        /// <summary>
        /// 解锁武器攻击力
        /// </summary>
        public  string  weaponInfo {get { return m_WeaponInfo; } }
       
        /// <summary>
        /// 敌人波次
        /// </summary>
        public  int  enemyCount {get { return m_EnemyCount; } }
       

        public void ReadRow(DataStreamReader dataR, int[] filedIndex)
        {
          //var schemeNames = dataR.GetSchemeName();
          int col = 0;
          while(true)
          {
            col = dataR.MoreFieldOnRow();
            if (col == -1)
            {
              break;
            }
            switch (filedIndex[col])
            { 
            
                case 0:
                    m_ID = dataR.ReadInt();
                    break;
                case 1:
                    m_SceneID = dataR.ReadInt();
                    break;
                case 2:
                    m_EnemyID = dataR.ReadInt();
                    break;
                case 3:
                    m_IsBoss = dataR.ReadBool();
                    break;
                case 4:
                    m_WeaponInfo = dataR.ReadString();
                    break;
                case 5:
                    m_EnemyCount = dataR.ReadInt();
                    break;
                default:
                    //TableHelper.CacheNewField(dataR, schemeNames[col], m_DataCacheNoGenerate);
                    break;
            }
          }

        }
        
        public static Dictionary<string, int> GetFieldHeadIndex()
        {
          Dictionary<string, int> ret = new Dictionary<string, int>(6);
          
          ret.Add("ID", 0);
          ret.Add("SceneID", 1);
          ret.Add("EnemyID", 2);
          ret.Add("IsBoss", 3);
          ret.Add("WeaponInfo", 4);
          ret.Add("EnemyCount", 5);
          return ret;
        }
    } 
}//namespace LR