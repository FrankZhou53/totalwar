﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

namespace GameWish.Game
{

    public class ToggleButton : MonoBehaviour
    {
        [SerializeField]
        private Toggle m_Toggle;
        [SerializeField]
        private Transform m_Bar;

        public Toggle Toggle
        {
            get { return m_Toggle; }
            set { m_Toggle = value; }
        }

        public event Action<bool> EventHandler;
        private void Awake()
        {
            Vector3 pos = m_Bar.transform.localPosition;
            pos.x = (Toggle.isOn ? 25 : -25);
            m_Bar.transform.localPosition = pos;
            m_Toggle.onValueChanged.AddListener((ison) =>
            {
                m_Bar.DOLocalMoveX(ison ? 25 : -25, 0.15f);
                EventHandler?.Invoke(ison);
            });

        }
    }
}