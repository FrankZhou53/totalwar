﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Qarth;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class GamingPanel : AbstractPanel
    {
        [SerializeField] private GameObject m_BtnStart;
        [SerializeField] private GameObject m_ObjSpecial;
        [SerializeField] private List<Button> m_LstBtnWeapon;
        [SerializeField] private Text m_TxtLevel;
        [SerializeField] private Text m_TxtKill;
        int killCount = 0;
        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_ObjSpecial.SetActive(false);
            //m_BtnStart.onClick.AddListener(OnClickStart);
            m_LstBtnWeapon[0].onClick.AddListener(OnClickWeapon1);
            m_LstBtnWeapon[1].onClick.AddListener(OnClickWeapon2);
            m_LstBtnWeapon[2].onClick.AddListener(OnClickWeapon3);
            m_LstBtnWeapon[3].onClick.AddListener(OnClickWeapon4);
            m_LstBtnWeapon[4].onClick.AddListener(OnClickWeapon5);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            m_BtnStart.gameObject.SetActive(true);
            m_TxtLevel.text = string.Format("Level {0}", PlayerInfoMgr.data.currentLevel + 1);
            m_TxtKill.gameObject.SetActive(false);
            killCount = 0;
            if (args.Length > 0)
            {
                m_ObjSpecial.SetActive((bool)args[0]);
                ShowNowWeapon(0);
                for (var i = 0; i < m_LstBtnWeapon.Count; i++)
                {
                    if (i < LevelMgr.S.config.dicWeaponInfo.Count)
                    {
                        m_LstBtnWeapon[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        m_LstBtnWeapon[i].gameObject.SetActive(false);
                    }
                }
                int wIndex = 0;
                foreach (var item in LevelMgr.S.config.dicWeaponInfo)
                {
                    var cLst = m_LstBtnWeapon[wIndex].GetComponentsInChildren<Image>(true);
                    for (var i = 0; i < cLst.Length; i++)
                    {
                        if (cLst[i].name == "icon")
                        {
                            cLst[i].sprite = this.FindSprite(string.Format("icon_{0}", item.Key));
                        }
                    }
                    wIndex++;
                }
            }
            //

            EventSystem.S.Register(EventID.OnKillEnemy, OnKillEnemy);
        }

        protected override void OnClose()
        {
            base.OnClose();
            EventSystem.S.UnRegister(EventID.OnKillEnemy, OnKillEnemy);
        }
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                m_BtnStart.gameObject.SetActive(false);
            }
        }
        void OnClickStart()
        {
            //m_BtnStart.gameObject.SetActive(false);
        }
        void OnClickWeapon1()
        {
            ShowNowWeapon(0);
        }
        void OnClickWeapon2()
        {
            ShowNowWeapon(1);
        }
        void OnClickWeapon3()
        {
            ShowNowWeapon(2);
        }
        void OnClickWeapon4()
        {
            ShowNowWeapon(3);
        }
        void OnClickWeapon5()
        {
            ShowNowWeapon(4);
        }
        void ShowNowWeapon(int _index)
        {
            BattleMgr.S.InitWeapon(_index);
            for (var i = 0; i < m_LstBtnWeapon.Count; i++)
            {
                m_LstBtnWeapon[i].transform.Find("ready").gameObject.SetActive(i != _index);
                m_LstBtnWeapon[i].transform.Find("choose").gameObject.SetActive(i == _index);
                m_LstBtnWeapon[i].interactable = i != _index;
            }
        }
        void OnKillEnemy(int key, params object[] args)
        {
            m_TxtKill.DOKill();
            killCount += 1;
            float showTime = 0;
            if (m_TxtKill.gameObject.activeSelf)
            {
                m_TxtKill.transform.localScale = Vector3.one;
                showTime = 0.05f;
            }
            else
            {
                m_TxtKill.transform.localScale = Vector3.zero;
                showTime = 0.2f;
            }
            m_TxtKill.text = string.Format("KILL {0}", killCount);
            m_TxtKill.gameObject.SetActive(true);
            m_TxtKill.color = Color.white;
            Sequence seq = DOTween.Sequence();
            seq.Append(m_TxtKill.transform.DOScale(Vector3.one * 1.2f, showTime));
            seq.Append(m_TxtKill.transform.DOScale(Vector3.one, 0.1f));
            seq.AppendInterval(2.0f);
            seq.Append(m_TxtKill.DOFade(0, 0.2f));
            seq.AppendCallback(() =>
            {
                m_TxtKill.gameObject.SetActive(false);
            });
        }
    }
}

