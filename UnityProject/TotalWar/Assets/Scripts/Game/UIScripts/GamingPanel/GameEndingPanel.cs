﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Qarth;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class GameEndingPanel : AbstractAnimPanel
    {
        [SerializeField] private Button m_BtnNext;
        [SerializeField] private GameObject m_ObjSuccess;
        [SerializeField] private GameObject m_ObjFaile;
        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnNext.onClick.AddListener(OnClickStart);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            if (args.Length > 0)
            {
                bool isSuccess = (bool)args[0];
                m_ObjSuccess.SetActive(isSuccess);
                m_ObjFaile.SetActive(!isSuccess);
            }
        }

        protected override void OnClose()
        {
            base.OnClose();

        }
        void OnClickStart()
        {
            CloseSelfPanel();
            BattleMgr.S.ClearBattle();
            LevelMgr.S.StartNewGame();
        }
    }
}

