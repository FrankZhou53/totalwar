﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class GuideWordsPanel : AbstractAnimPanel
    {
        [SerializeField]
        private GameObject m_ObjGuide;
        [SerializeField]
        private Image m_ImgGuideHand;

        protected override void OnUIInit()
        {
            base.OnUIInit();
        }

        protected override void OnPanelOpen(params object[] args)
        {
            m_ObjGuide.gameObject.SetActive(false);
            m_ImgGuideHand.gameObject.SetActive(false);
            if (args == null || args.Length == 0)
            {
                return;
            }

            if (args.Length > 1)
            {
                m_ObjGuide.gameObject.SetActive(true);
                m_ObjGuide.GetComponentInChildren<Text>().text = TDLanguageTable.Get(args[1].ToString());
            }

            if (args.Length > 2)
            {
                Transform trasTarget = (args[2] as IUINodeFinder).FindNode(false) as RectTransform;
                m_ObjGuide.transform.position = trasTarget.position;
            }

            if (args.Length > 3)
            {
                m_ObjGuide.transform.localPosition += Helper.String2Vector3(args[3].ToString(), '|');
            }

            if (args.Length > 4)
            {
                m_ImgGuideHand.gameObject.SetActive(true);
                Transform trasTarget1 = (args[4] as IUINodeFinder).FindNode(false) as RectTransform;
                m_ImgGuideHand.transform.position = trasTarget1.position;
                if (args.Length > 5)
                {
                    Transform trasTarget2 = (args[5] as IUINodeFinder).FindNode(false) as RectTransform;
                    m_ImgGuideHand.transform.DOMove(trasTarget2.position, 1f).SetEase(Ease.Linear)
                      .SetLoops(-1, LoopType.Restart);
                }
            }
        }


        protected override void OnPanelHideComplete()
        {
            base.OnPanelHideComplete();
            CloseSelfPanel();
        }

    }
}

