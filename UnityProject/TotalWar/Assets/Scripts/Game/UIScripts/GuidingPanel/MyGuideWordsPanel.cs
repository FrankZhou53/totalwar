﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class MyGuideWordsPanel : AbstractAnimPanel
    {
        [SerializeField] private GameObject m_ObjGuide;
        [SerializeField] private Text m_TxtTip;

        private Transform m_Target;
        protected Vector3 m_OffsetPos = Vector3.zero;
        protected Vector3 m_OldPos = Vector3.zero;
        protected Vector3 m_MoveToPos = new Vector3(0, 15, 0);

        protected override void OnUIInit()
        {
            base.OnUIInit();
        }

        protected override void OnPanelOpen(params object[] args)
        {
            m_ObjGuide.gameObject.SetActive(false);
            if (args == null || args.Length == 0)
            {
                return;
            }

            m_ObjGuide.gameObject.SetActive(true);
            m_TxtTip.text = TDLanguageTable.Get(args[1].ToString());

            if (args.Length > 2)
            {
                if (!args[2].ToString().Equals("null"))
                    m_Target = (args[2] as IUINodeFinder).FindNode(false) as RectTransform;
                // m_ObjGuide.transform.position = m_Target.position;
            }

            if (args.Length > 3)
            {
                if (!args[3].ToString().Equals("null"))
                    m_OffsetPos = Helper.String2Vector3(args[3].ToString(), '|');
                // m_ObjGuide.transform.localPosition += Helper.String2Vector3(args[3].ToString(), '|');
            }

            Update();
        }


        private void Update()
        {
            if (m_Target == null)
            {

                return;
            }

            Vector3 pos = GetTipPos();
            // if (pos.x != m_OldPos.x || pos.y != m_OldPos.y)
            {
                m_ObjGuide.transform.position = pos;

                var localpos = m_ObjGuide.transform.localPosition;
                localpos = localpos + m_OffsetPos;
                float lposX = Mathf.Clamp(localpos.x, -200f, 200f);
                localpos = new Vector3(lposX, localpos.y, localpos.z);
                m_ObjGuide.transform.localPosition = localpos;

                m_ObjGuide.transform.DOKill();

                if (m_MoveToPos != Vector3.zero)
                {
                    m_ObjGuide.transform.DOLocalMove((localpos + m_MoveToPos), 1f).SetLoops(-1)
                        .SetEase(Ease.Linear);
                }

                m_OldPos = pos;
            }
        }

        private Vector3 GetTipPos()
        {
            return m_Target.position;
        }


        protected override void OnPanelHideComplete()
        {
            base.OnPanelHideComplete();
            CloseSelfPanel();
        }

    }
}

