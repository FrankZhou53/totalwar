﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public class SplashPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_BtnClose;
        [SerializeField]
        private Text m_TxtClose;
        private int m_CloseTimer = -1;
        private int m_CloseDuration = 3;
        // [SerializeField]
        // private AdMixViewView m_AdView;

        private Action m_Listener;

        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnClose.onClick.AddListener(OnClickClose);
            // m_AdView.BindAdInterface("SplashMixView");
            // m_AdView.SetRetryParams(1, 2);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                m_Listener = args[0] as Action;
            }


            // var yPos = Mathf.Abs((int)m_AdView.GetComponent<RectTransform>().anchoredPosition.y);
            // m_AdView.ShowAd(0, yPos, TaurusXAdSdk.Api.AdPosition.Center);

            if (m_CloseTimer > 0)
            {
                Timer.S.Cancel(m_CloseTimer);
                m_CloseTimer = -1;
            }
            m_CloseTimer = Timer.S.Post2Really(OnCloseTick, 1, m_CloseDuration);
            m_TxtClose.text = m_CloseDuration.ToString();
        }

        protected override void OnClose()
        {
            base.OnClose();
            // m_AdView.HideAd();
            if (m_CloseTimer > 0)
            {
                Timer.S.Cancel(m_CloseTimer);
                m_CloseTimer = -1;
            }
        }


        void OnCloseTick(int count)
        {
            var left = Mathf.Max(0, m_CloseDuration - count);
            if (left <= 0)
            {
                m_TxtClose.text = "0";
                Timer.S.Cancel(m_CloseTimer);
                m_CloseTimer = -1;
                OnClickClose();
            }
            else
            {
                m_TxtClose.text = left.ToString();
            }
        }

        void OnClickClose()
        {
            CloseSelfPanel();
            if (m_Listener != null)
            {
                m_Listener();
                m_Listener = null;
            }
        }
    }
}
