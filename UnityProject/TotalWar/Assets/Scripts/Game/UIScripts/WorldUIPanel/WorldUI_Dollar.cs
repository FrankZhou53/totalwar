using DG.Tweening;
using Qarth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameWish.Game;

public class WorldUI_Dollar : WorldUIBindTransform
{
    [SerializeField] private Text m_Label;

    void Awake()
    {
        transform.localScale = Vector3.zero;
    }

    public void SetValue(float value, bool big = false)
    {
        m_Label.color = Color.white;
        m_Label.text = string.Format("${0:N2}", value);

        var scale = big ? 0.8f : 0.4f;
        var duration = big ? 0.5f : 0.2f;

        transform.DOKill();
        transform.localScale = Vector3.zero;
        transform.DOScale(scale, 0.2f).SetEase(Ease.OutBack).OnComplete(() =>
        {
            m_Label.DOKill();
            transform.DOLocalMoveY(transform.localPosition.y + 10, duration);
            m_Label.DOFade(0, duration).OnComplete(() =>
            {
                GameObjectPoolMgr.S.Recycle(gameObject);
            });
        });
    }
}
