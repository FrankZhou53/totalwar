﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{

    public class WorldUIPanel : AbstractPanel
    {
        public static WorldUIPanel S;

        [SerializeField] private WorldUI_Tokens m_TokensSmall;
        [SerializeField] private WorldUI_Dollar m_DollarSmall;
        [SerializeField] private Text m_TxtBonus;

        protected override void OnUIInit()
        {
            base.OnUIInit();
            S = this;
            GameObjectPoolMgr.S.AddPool("WorldUI_Tokens", m_TokensSmall.gameObject, -1, 3);
            GameObjectPoolMgr.S.AddPool("WorldUI_Dollar", m_DollarSmall.gameObject, -1, 3);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            m_TxtBonus.transform.localScale = Vector3.zero;
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        public void AllocateTokensUI(Transform position, int Tokens, bool big = false)
        {
            var go = GameObjectPoolMgr.S.Allocate("WorldUI_Tokens").GetComponent<WorldUI_Tokens>();

            if (go != null)
            {
                go.followTransform = position;

                go.targetUI = go.transform;
                go.transform.SetParent(transform);
                go.SetValue(Tokens, big);
            }
        }

        public void AllocateDolloarUI(Transform position, float dol, bool big = false)
        {
            var go = GameObjectPoolMgr.S.Allocate("WorldUI_Dollar").GetComponent<WorldUI_Dollar>();

            if (go != null)
            {
                go.followTransform = position;

                go.targetUI = go.transform;
                go.transform.SetParent(transform);
                go.SetValue(dol, big);
            }
        }

        public void ShowBonusText()
        {
            m_TxtBonus.transform.DOKill();
            m_TxtBonus.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutElastic).OnComplete(() =>
            {
                m_TxtBonus.transform.DOKill();
                m_TxtBonus.transform.DOScale(Vector3.zero, 0.3f).SetDelay(0.5f);
            });
        }

    }
}
