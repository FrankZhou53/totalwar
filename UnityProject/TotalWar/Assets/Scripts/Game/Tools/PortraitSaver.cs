using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using System.IO;

namespace GameWish.Game
{
    public class PortraitSaver
    {
        //保存
        public static bool Save(RenderTexture rt, string filename)
        {
            if (!filename.ToLower().EndsWith(".png"))
            {
                filename += ".png";
            }

            RenderTexture prev = RenderTexture.active;
            RenderTexture.active = rt;
            Texture2D png = new Texture2D(rt.width, rt.height, TextureFormat.ARGB32, false);
            png.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            byte[] bytes = png.EncodeToPNG();
            if (!Directory.Exists(persistentDataPath4Portrait))
                Directory.CreateDirectory(persistentDataPath4Portrait);

            var filepath = Path.Combine(persistentDataPath4Portrait, filename);

            FileStream file = File.Open(filepath, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(file);
            writer.Write(bytes);
            file.Close();
            Texture2D.DestroyImmediate(png);
            png = null;
            RenderTexture.active = prev;
            return true;
        }

        public static Sprite GetSprite(RenderTexture rt)
        {
            RenderTexture prev = RenderTexture.active;
            Texture2D texture2D = new Texture2D(rt.width, rt.height, TextureFormat.ARGB32, false);
            RenderTexture.active = rt;
            texture2D.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            texture2D.Apply();

            RenderTexture.active = prev;
            return Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), Vector2.zero);
        }

        public static void SaveSprite(Sprite sp, string filename)
        {
            if (!filename.ToLower().EndsWith(".png"))
            {
                filename += ".png";
            }
            Texture2D png = sp.texture;

            byte[] bytes = png.EncodeToPNG();
            if (!Directory.Exists(persistentDataPath4Portrait))
                Directory.CreateDirectory(persistentDataPath4Portrait);

            var filepath = Path.Combine(persistentDataPath4Portrait, filename);

            FileStream file = File.Open(filepath, FileMode.Create);
            BinaryWriter writer = new BinaryWriter(file);
            writer.Write(bytes);
            file.Close();
            Texture2D.DestroyImmediate(png);
            //png = null;
        }


        //删除
        public static void Delete(string filename)
        {
            if (!filename.ToLower().EndsWith(".png"))
            {
                filename += ".png";
            }
            var filepath = Path.Combine(persistentDataPath4Portrait, filename);
            if (File.Exists(filepath))
                File.Delete(filepath);
        }

        private static byte[] GetImageByte(string filename)
        {
            if (!filename.ToLower().EndsWith(".png"))
            {
                filename += ".png";
            }

            var path = Path.Combine(persistentDataPath4Portrait, filename);
            if (!File.Exists(path))
                return null;
            FileStream files = new FileStream(path, FileMode.Open);
            //新建比特流对象
            byte[] imgByte = new byte[files.Length];
            //将文件写入对应比特流对象
            files.Read(imgByte, 0, imgByte.Length);
            //关闭文件
            files.Close();
            //返回比特流的值
            return imgByte;
        }

        //读取到sprite
        public static Sprite LoadTexture2Sprite(string filename)
        {
            Texture2D t2d = new Texture2D(512, 512);
            var bytes = GetImageByte(filename);
            if (bytes == null)
                return null;
            t2d.LoadImage(bytes);
            //将Texture创建成Sprite 参数分别为图片源文件,Rect值给出起始点和大小 以及锚点的位置
            return Sprite.Create(t2d, new Rect(0, 0, t2d.width, t2d.height), Vector2.zero);
        }


        private static string m_PersistentDataPath4Portrait;
        // 外部资源目录
        public static string persistentDataPath4Portrait
        {
            get
            {
                if (null == m_PersistentDataPath4Portrait)
                {
                    m_PersistentDataPath4Portrait = FilePath.persistentDataPath + "portrait/";

                    if (!Directory.Exists(m_PersistentDataPath4Portrait))
                    {
                        Directory.CreateDirectory(m_PersistentDataPath4Portrait);
#if UNITY_IPHONE && !UNITY_EDITOR
                        UnityEngine.iOS.Device.SetNoBackupFlag(m_PersistentDataPath4Portrait);
#endif
                    }
                }

                return m_PersistentDataPath4Portrait;
            }
        }
    }
}
