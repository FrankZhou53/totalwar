﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class UIRootMgr : TMonoSingleton<UIRootMgr>
    {
        [SerializeField]
        private Transform m_PanelRoot;
        // Start is called before the first frame update
        public override void OnSingletonInit()
        {

        }
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {

        }
    }
}