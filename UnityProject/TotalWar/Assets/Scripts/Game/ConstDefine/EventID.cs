﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum EventID
    {
        //guide
        OnPanelClose,
        OnLanguageTableSwitchFinish,
        OnTaskAdd,
        ShowGuideLine,
        AddWord,
        UnLockPicBtn,
        OnGuidePropertyComplete,
        TaskStateChanged,

        //ui
        OnPropertyAdd,
        OnCardGet,
        OnCollectionGet,

        //scene
        OnSceneLoaded,

        //
        OnFireBullet,
        OnKillEnemy,
        OnWeaponReady,
        OnEnemyEndingPos,
        OnEnemyEndingPosDead,
        OnGameOver,
        OnGameComplete,
    }
}
