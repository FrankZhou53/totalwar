﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class BKunai : BulletBase
    {
        [SerializeField] private Animation m_Anim;
        protected override bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            m_Anim.Stop();
            return base.HurtEnemy(_enemy, _force);
        }
        protected override void HurtGround()
        {
            m_Anim.Stop();
        }

    }
}
