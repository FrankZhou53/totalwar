﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class BMace : BulletBase
    {
        protected override void InitOther()
        {
            m_TrsOther.transform.eulerAngles += new Vector3(RandomHelper.Range(-30, 30f), RandomHelper.Range(-30, 30f), RandomHelper.Range(0, 360f));
        }
        protected override bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            return base.HurtEnemy(_enemy, _force);
        }
        protected override void HurtGround()
        {

        }
        protected override void OnCollisionEnter(Collision other)
        {
            isNew = false;
            if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))    //将检测结果的碰撞对象标签与player标签对比.判断是否相等
            {
                m_relDesTime = RandomHelper.Range(3.5f, 4.5f);
                var force = m_Rigidbody.velocity;
                //获取敌人
                GameObject body = other.gameObject;
                while (body.layer == LayerMask.NameToLayer("Enemy"))
                {
                    body = body.transform.parent.gameObject;
                }
                if (HurtEnemy(body, force))
                {
                    //EventSystem.S.Send(EventID.OnKillEnemy);
                }
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                m_relDesTime = RandomHelper.Range(2f, 4f);
                SetPhysics(false);
                this.transform.eulerAngles = currentAngle;
                HurtGround();
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Ice"))
            {
                var enemy = other.transform.GetComponent<RoleBase>();
                enemy.Broken();
            }
        }
    }
}
