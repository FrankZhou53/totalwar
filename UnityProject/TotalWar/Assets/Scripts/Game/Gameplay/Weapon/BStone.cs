﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class BStone : BulletBase
    {
        private float realHurt;
        public override void Init(float _atk = -1)
        {
            realHurt = _atk > 0 ? _atk : m_Hurt;
            this.transform.localScale = Vector3.one;
            isRecysle = false;
            isNew = true;
            m_relDesTime = destoryTime;
            liveTime = 0;
            InitOther();
            currentAngle = this.transform.eulerAngles;
            m_Rigidbody = this.GetComponent<Rigidbody>();
            SetPhysics(true);
            InitEffect();
        }
        protected override void InitEffect()
        {
            m_Hurt = realHurt;
        }
        protected override void OnCollisionEnter(Collision other)
        {
            isNew = false;
            if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))    //将检测结果的碰撞对象标签与player标签对比.判断是否相等
            {
                m_relDesTime = RandomHelper.Range(3.5f, 4.5f);
                var force = m_Rigidbody.velocity;
                //获取敌人
                GameObject body = other.gameObject;
                while (body.layer == LayerMask.NameToLayer("Enemy"))
                {
                    body = body.transform.parent.gameObject;
                }
                if (HurtEnemy(body, force))
                {
                    //EventSystem.S.Send(EventID.OnKillEnemy);
                }
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                //m_Hurt = 0;
                m_relDesTime = RandomHelper.Range(2f, 4f);
                //SetPhysics(false);
                //this.transform.eulerAngles = currentAngle;
                HurtGround();
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Ice"))
            {
                var enemy = other.transform.GetComponent<RoleBase>();
                enemy.Broken();
            }
        }
    }
}
