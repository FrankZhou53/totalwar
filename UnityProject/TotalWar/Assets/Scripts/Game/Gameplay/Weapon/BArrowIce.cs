﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class BArrowIce : BulletBase
    {
        [SerializeField] private GameObject m_IceHurt;
        protected override void InitEffect()
        {
            //m_IceHurt.SetActive(false);
            //m_IceBomb.SetActive(false);
        }
        protected override bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            //m_IceHurt.SetActive(true);
            return _enemy.GetComponent<RoleBase>().Hurted(_force * m_Force, m_Hurt, Define.AttackType.Ice);
        }
        protected override void HurtGround()
        {
            //m_IceHurt.SetActive(true);
        }
        public override void RecycleSelf()
        {
            base.RecycleSelf();
            InitEffect();
        }
        protected override void OnCollisionEnter(Collision other)
        {
            base.OnCollisionEnter(other);

        }
    }
}
