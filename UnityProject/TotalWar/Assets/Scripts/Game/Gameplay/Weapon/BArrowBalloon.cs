﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class BArrowBalloon : BulletBase
    {
        [SerializeField] private GameObject m_balloon;
        private bool isHurted = false;
        private GameObject m_Enemy;
        protected override void InitEffect()
        {
            m_TrsOther.gameObject.SetActive(true);
            m_balloon.transform.DOKill();
            m_balloon.SetActive(false);
        }
        protected override bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            _enemy.GetComponent<RoleBase>().Hurted(_force * m_Force, m_Hurt, Define.AttackType.Balloon);
            isHurted = true;
            m_Enemy = _enemy;
            var vel = new Vector3(RandomHelper.Range(-180f, 180f), RandomHelper.Range(0, 180f), RandomHelper.Range(-180f, 180f)).normalized * m_Force;
            m_Enemy.GetComponent<RoleBase>().ChangeVelocity(vel);
            ShowBalloon();
            return true;

        }
        private void ShowBalloon()
        {
            //m_TrsOther.gameObject.SetActive(false);
            m_balloon.transform.localScale = Vector3.zero;
            m_balloon.SetActive(true);
            m_balloon.transform.DOScale(Vector3.one * RandomHelper.Range(0.8f, 1f), 0.2f);
            //this.transform.eulerAngles = Vector3.up;
        }
        private void ChangeEnemyVelocity()
        {
            m_Enemy.GetComponent<RoleBase>().ChangeVelocity(Vector3.up * m_Force);
        }
        protected override void HurtGround()
        {
            ShowBalloon();
        }
        protected override void UpdateBullet()
        {
            if (isHurted)
            {
                ChangeEnemyVelocity();
            }
            m_balloon.transform.eulerAngles = Vector3.zero;
        }
        public override void RecycleSelf()
        {
            base.RecycleSelf();
            InitEffect();
            isHurted = false;
            if (m_Enemy != null)
            {
                m_Enemy.GetComponent<RoleBase>().HurtOver(Define.AttackType.Balloon);
            }
        }
    }
}
