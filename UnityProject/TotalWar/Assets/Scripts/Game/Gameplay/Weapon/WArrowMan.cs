﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class WArrowMan : WeaponBase
    {
        [SerializeField] private GameObject m_ShowBullet;
        [SerializeField] private Animation m_Anim;
        protected override float ShowFireAction()
        {
            m_Anim.Play("ArrowMenAttack02");
            m_ShowBullet.SetActive(false);
            m_FireBase.gameObject.SetActive(false);
            return base.ShowFireAction();
        }
        protected override void ShowReadyFireAction()
        {
            base.ShowReadyFireAction();
            m_Anim.Play("ArrowMenAttack01");
            Timer.S.Post2Really((int time) =>
                {
                    m_ShowBullet.SetActive(true);
                    m_FireBase.gameObject.SetActive(true);
                }, 0.333f, 1);
        }
    }
}
