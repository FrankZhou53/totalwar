﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class WeaponBase : MonoBehaviour
    {
        [SerializeField] protected GameObject m_Bullet;
        [SerializeField] protected Transform m_FirePosRoot;
        [SerializeField] protected Transform m_ForwardPos;
        [SerializeField] protected Transform m_FireBase;
        [SerializeField] protected Transform m_WeaponBase;
        [SerializeField] protected float m_ScaleDelayTime = 0.2f;
        [SerializeField] protected Transform m_BaseFirePos;
        [SerializeField] protected Transform m_Complicated;
        [SerializeField] protected int m_ShootCount = 1;
        [SerializeField] protected float m_Interval = 0.5f;
        public float yOffset = 0f;
        public float speed = 20;
        public float cd = 3;

        protected float cd_time = 0;
        public float minPortraitAngles = 0;
        public float maxPortraitAngles = 0;

        public float minHorizontalAngles = 0;
        public float maxHorizontalAngles = 0;
        protected List<Transform> m_LstFirePos;
        protected List<Vector3> m_LstFireForward = new List<Vector3>();
        protected bool canFire = true;
        protected Vector3 start_pos;
        protected float realAtk = -1;

        public void Init(float atk = -1)
        {
            realAtk = atk;
            m_LstFirePos = m_FirePosRoot.GetChildTrsList();
            foreach (var item in m_LstFirePos)
            {
                m_LstFireForward.Add((item.position - m_ForwardPos.position).normalized);
            }
            GameObjectPoolMgr.S.AddPool(m_Bullet.name, m_Bullet, -1, m_LstFirePos.Count * m_ShootCount * 2);
            ShowComplicatedLine();
            ShowReadyFireAction();
        }
        public void ClearWeapon()
        {
            GameObjectPoolMgr.S.RemovePool(m_Bullet.name, false);
            Destroy(this.gameObject);
        }
        // Update is called once per frame
        void Update()
        {
            m_Complicated.LookAt(GameCamMgr.S.transform);

            cd_time -= Time.deltaTime;
            if (cd_time <= 0)
            {
                if (!canFire)
                {
                    ShowReadyFireAction();
                }
            }

            if (BattleMgr.S.isOver)
            {
                return;
            }

            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) return;

            if (Input.GetMouseButtonDown(0))
            {
                start_pos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                float move_y = (Input.mousePosition.y - start_pos.y) * 4;
                float anglesX = m_FireBase.localEulerAngles.x > 180 ? m_FireBase.localEulerAngles.x - 360 : m_FireBase.localEulerAngles.x;
                if (move_y < 0 && anglesX <= maxPortraitAngles)//向上调整
                {
                    m_FireBase.localEulerAngles += new Vector3(Time.deltaTime * (-move_y), 0, 0);
                }
                else if (move_y > 0 && anglesX >= minPortraitAngles)//向下调整
                {
                    m_FireBase.localEulerAngles -= new Vector3(Time.deltaTime * move_y, 0, 0);
                }

                float move_x = (Input.mousePosition.x - start_pos.x) * 4;
                float anglesY = m_WeaponBase.localEulerAngles.y > 180 ? m_WeaponBase.localEulerAngles.y - 360 : m_WeaponBase.localEulerAngles.y;
                if (move_x < 0 && anglesY >= minHorizontalAngles)//向右调整
                {
                    m_WeaponBase.localEulerAngles -= new Vector3(0, Time.deltaTime * (-move_x), 0);
                }
                else if (move_x > 0 && anglesY <= maxHorizontalAngles)//向左调整……全部改为反向了
                {
                    m_WeaponBase.localEulerAngles += new Vector3(0, Time.deltaTime * move_x, 0);
                }


                start_pos = Input.mousePosition;
            }
            else if (Input.GetKeyDown(KeyCode.J) || Input.GetMouseButtonUp(0))
            {
                if (canFire)
                {
                    cd_time = cd;
                    OpenFire();
                }
            }
            ShowComplicatedLine();
        }
        protected void OnDrawGizmos()
        {
            // var vec = (m_BaseFirePos.position - m_ForwardPos.position).normalized;
            // Gizmos.DrawWireSphere(m_ForwardPos.position + vec * ShowComplicatedLine(), 0.5f);
        }
        protected float ShowComplicatedLine()
        {
            //瞄准线
            float enemyDis = 15;
            var vec = (m_BaseFirePos.position - m_ForwardPos.position).normalized;
            Collider[] cols = Physics.OverlapCapsule(m_ForwardPos.position, m_ForwardPos.position + vec * 20, 0.5f, LayerMask.GetMask("Enemy"));
            if (cols.Length > 0)
            {
                //Transform enemy = null;
                foreach (var item in cols)
                {
                    var dis = Vector3.Distance(item.transform.position, m_ForwardPos.position);
                    if (dis < enemyDis)
                    {
                        enemyDis = dis;
                    }
                }
            }
            //校准是否瞄准地面
            Ray ray = new Ray(m_ForwardPos.position, vec);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 15, LayerMask.GetMask("Ground")))
            {
                var gDis = Vector3.Distance(hit.point, m_ForwardPos.position);
                if (gDis < enemyDis)
                {
                    enemyDis = gDis;
                }
            }
            //绘制瞄准线
            m_Complicated.position = m_ForwardPos.position + vec * enemyDis + (cols.Length > 0 ? Vector3.zero : (Vector3.down * yOffset));
            return enemyDis;
        }
        protected virtual void OpenFire()
        {
            StartCoroutine(CreatBullets(m_LstFirePos.Count));
        }
        protected virtual IEnumerator CreatBullets(int _count)
        {
            for (var j = 0; j < m_ShootCount; j++)
            {
                ShowFireAction();
                for (var i = 0; i < _count; i++)
                {
                    var bullet = GameObjectPoolMgr.S.Allocate(m_Bullet.name).GetComponent<BulletBase>();
                    bullet.transform.position = m_LstFirePos[i].position + new Vector3(0, RandomHelper.Range(-0.2f, 0.2f), RandomHelper.Range(-0.2f, 0.2f));
                    bullet.transform.LookAt(m_ForwardPos);
                    //bullet.transform.rotation = 
                    bullet.transform.parent = BattleMgr.S.bulletRoot;
                    bullet.Init(realAtk);
                    bullet.gameObject.SetActive(true);
                    bullet.SetSpeed(m_ForwardPos.transform.TransformDirection(m_LstFireForward[i] * RandomHelper.Range(speed - 2, speed)));
                }
                float angles = m_WeaponBase.localEulerAngles.y > 180 ? (m_WeaponBase.localEulerAngles.y - 360f) : m_WeaponBase.localEulerAngles.y;
                float movePercent = (angles + Mathf.Abs(maxHorizontalAngles)) / (Mathf.Abs(maxHorizontalAngles) * 2) * 100;
                EventSystem.S.Send(EventID.OnFireBullet, m_ScaleDelayTime, (int)movePercent);
                yield return new WaitForSeconds(m_Interval);
            }
        }
        /// <summary>
        /// 武器发射动画
        /// </summary>
        protected virtual float ShowFireAction()
        {
            canFire = false;
            return 0;
        }
        /// <summary>
        /// 上弹动画
        /// </summary>
        protected virtual void ShowReadyFireAction()
        {
            EventSystem.S.Send(EventID.OnWeaponReady);
            canFire = true;
        }
    }
}
