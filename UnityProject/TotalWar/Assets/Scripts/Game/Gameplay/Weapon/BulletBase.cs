﻿using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class BulletBase : MonoBehaviour
    {
        [SerializeField] public Define.AttackType htype = Define.AttackType.Normal;
        [SerializeField] protected Transform m_TrsOther;
        [SerializeField] protected float m_Hurt = 10;
        [SerializeField] protected int m_Force = 3000;
        public float destoryTime = 4f;
        protected float m_relDesTime;
        protected float liveTime = 0;
        protected float hurtTime = 0;
        protected bool isNew = true;
        protected Vector3 currentAngle;
        protected Rigidbody m_Rigidbody;
        protected bool isRecysle = false;
        // Start is called before the first frame update
        public virtual void Init(float _atk = -1)
        {
            m_Hurt = _atk == -1 ? m_Hurt : _atk;
            //destoryTime = RandomHelper.Range(3.5f, 4.5f);
            this.transform.localScale = Vector3.one;
            isRecysle = false;
            isNew = true;
            m_relDesTime = destoryTime;
            liveTime = 0;
            InitOther();
            currentAngle = this.transform.eulerAngles;
            m_Rigidbody = this.GetComponent<Rigidbody>();
            SetPhysics(true);
            InitEffect();
        }
        public virtual void SetSpeed(Vector3 _speed)
        {
            this.GetComponent<Rigidbody>().velocity = _speed;//m_ForwardPos.transform.TransformDirection(m_LstFireForward[i] * RandomHelper.Range(speed - 2, speed));
        }
        // Update is called once per frame
        void Update()
        {
            liveTime += Time.deltaTime;
            //this.GetComponent<Rigidbody>().centerOfMass = m_TrsFocus.localPosition;
            if (liveTime >= m_relDesTime - 0.15f && !isRecysle)
            {
                isRecysle = true;
                this.transform.DOScale(Vector3.zero, 0.15f);
                //RecycleSelf();
            }
            if (liveTime >= m_relDesTime)
            {
                RecycleSelf();
            }
            UpdateBullet();
        }
        void FixedUpdate()
        {
            if (isNew && m_Rigidbody.velocity.x != 0)
            {
                //Log.e(currentAngle);
                currentAngle.x = -Mathf.Atan(m_Rigidbody.velocity.y / m_Rigidbody.velocity.x) * Mathf.Rad2Deg;
                this.transform.eulerAngles = currentAngle;
            }
            FixedUpdateBullet();
        }
        protected virtual void InitOther()
        {
            m_TrsOther.transform.eulerAngles += new Vector3(0, 0, RandomHelper.Range(0, 360f));
        }
        protected virtual void InitEffect()
        {

        }
        protected virtual void UpdateBullet()
        {


        }
        protected virtual void FixedUpdateBullet()
        {


        }
        public virtual void RecycleSelf()
        {
            this.gameObject.SetActive(false);
            GameObjectPoolMgr.S.Recycle(this.gameObject);
            DestoryShow();
        }

        protected virtual bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            return _enemy.GetComponent<RoleBase>().Hurted(_force * m_Force, m_Hurt);
        }
        protected virtual void HurtGround()
        {

        }
        protected virtual void DestoryShow()
        {

        }
        protected virtual void OnCollisionEnter(Collision other)
        {
            isNew = false;
            if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))    //将检测结果的碰撞对象标签与player标签对比.判断是否相等
            {
                m_relDesTime = RandomHelper.Range(3.5f, 4.5f);
                var force = m_Rigidbody.velocity;
                this.transform.parent = other.transform;
                this.transform.localPosition = Vector3.zero;
                hurtTime = m_relDesTime - liveTime;
                //获取敌人
                GameObject body = other.gameObject;
                while (body.layer == LayerMask.NameToLayer("Enemy"))
                {
                    body = body.transform.parent.gameObject;
                }
                if (HurtEnemy(body, force))
                {
                    //other.gameObject.GetComponent<Rigidbody>().AddForce(force * 2000);
                    //EventSystem.S.Send(EventID.OnKillEnemy);
                }
                SetPhysics(false);
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                m_relDesTime = RandomHelper.Range(2f, 4f);
                SetPhysics(false);
                this.transform.eulerAngles = currentAngle;
                HurtGround();
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Ice"))
            {
                var enemy = other.transform.GetComponent<RoleBase>();
                enemy.Broken();
                //other.gameObject.SetActive(false);
                // var ice = GameObject.Instantiate(m_IceRole, BattleMgr.S.enemyRoot);
                // ice.transform.position = other.transform.position;
                // ice.transform.rotation = other.transform.rotation;
                //GameObjectPoolMgr.S.Recycle(other.gameObject);
                //m_IceBomb.SetActive(true);
                //Destroy(other.gameObject);
            }
        }
        protected void SetPhysics(bool _isOpen)
        {
            if (!_isOpen)
                m_Rigidbody.velocity = Vector3.zero;
            m_Rigidbody.isKinematic = !_isOpen;
            this.GetComponent<Collider>().isTrigger = !_isOpen;
            //m_TrsOther.GetComponent<Collider>().isTrigger = !_isOpen;
            //this.gameObject.layer = LayerMask.NameToLayer(_isOpen ? "Bullet" : "BulletOther");
        }
    }
}
