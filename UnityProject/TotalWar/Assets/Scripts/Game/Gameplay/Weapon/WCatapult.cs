﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class WCatapult : WeaponBase
    {
        [SerializeField] private GameObject m_ShowBullet;
        [SerializeField] private Transform m_Arm;
        protected override float ShowFireAction()
        {
            m_Arm.localEulerAngles = new Vector3(15, 0, 0);
            m_ShowBullet.SetActive(true);
            m_Complicated.gameObject.SetActive(false);
            Sequence seq = DOTween.Sequence();
            seq.Append(m_Arm.transform.DOLocalRotate(new Vector3(60, 0, 0), 0.1f));
            seq.AppendCallback(() =>
            {
                m_ShowBullet.SetActive(false);
            });
            return base.ShowFireAction();
        }
        protected override void ShowReadyFireAction()
        {
            base.ShowReadyFireAction();
            Sequence seq = DOTween.Sequence();
            seq.Append(m_Arm.transform.DOLocalRotate(new Vector3(15, 0, 0), 0.2f));
            seq.AppendCallback(() =>
            {
                m_Complicated.gameObject.SetActive(true);
                m_ShowBullet.SetActive(true);
            });
        }
    }
}
