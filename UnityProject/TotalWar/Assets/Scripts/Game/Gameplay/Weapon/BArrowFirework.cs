﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class BArrowFirework : BulletBase
    {
        [SerializeField] private GameObject m_FireworkBomb;
        [SerializeField] private GameObject m_FirewordGroundBomb;
        [SerializeField] private GameObject m_FireTrail;
        [SerializeField] private GameObject m_HurtTrail;
        [SerializeField] private GameObject m_Nromal;
        [SerializeField] private GameObject m_Bomb;
        public float forceTime = 0.1f;
        private float delayTime = 0;
        private bool isHurted = false;
        private bool isGround = false;
        private GameObject m_Enemy;
        private bool isBomb = false;
        protected override void InitEffect()
        {
            isBomb = false;
            isHurted = false;
            isGround = false;
            delayTime = 0;
            m_FireworkBomb.SetActive(false);
            m_FirewordGroundBomb.SetActive(false);
            m_FireTrail.SetActive(true);
            m_HurtTrail.SetActive(false);
            m_Nromal.SetActive(true);
            m_Bomb.SetActive(false);
            m_Enemy = null;
        }
        protected override bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            _enemy.GetComponent<RoleBase>().Hurted(_force * m_Force * 0, 0, Define.AttackType.Firework);
            m_Enemy = _enemy;
            // {
            //     isHurted = true;
            //     m_Enemy = _enemy;
            //     m_FireTrail.SetActive(false);
            //     m_HurtTrail.SetActive(true);
            //     //ChangeEnemyVelocity();
            //     return true;
            // }
            Bomb();
            return false;
        }
        private void ChangeEnemyVelocity()
        {
            var vel = new Vector3(RandomHelper.Range(-180f, 180f), RandomHelper.Range(0, 180f), RandomHelper.Range(-180f, 180f)).normalized * m_Force;
            m_Enemy.GetComponent<RoleBase>().ChangeVelocity(vel);
        }
        protected override void HurtGround()
        {
            isGround = true;
            //m_FirewordGroundBomb.SetActive(true);
        }
        protected override void UpdateBullet()
        {
            if (isHurted)
            {
                // delayTime += Time.deltaTime;
                // if (delayTime > forceTime)
                // {
                //     delayTime = 0;
                //     ChangeEnemyVelocity();
                // }
                // if (liveTime >= m_relDesTime - 1f)
                // {
                //     this.transform.parent = BattleMgr.S.transform;
                //     m_Nromal.SetActive(false);
                //     m_Bomb.SetActive(true);
                //     m_FireworkBomb.SetActive(true);
                //     if (m_Enemy != null)
                //     {
                //         var vel = new Vector3(RandomHelper.Range(-180f, 180f), RandomHelper.Range(-180, 180f), RandomHelper.Range(-180f, 180f)).normalized * m_Force * 2;
                //         m_Enemy.GetComponent<RoleBase>().ChangeVelocity(vel);
                //     }
                // }
                // if (m_Enemy == null)
                // {
                //     RecycleSelf();
                // }
                // m_HurtTrail.transform.eulerAngles = Vector3.right * 90;
            }
            else
            {
                if (liveTime >= 2f)//m_relDesTime - 1f)
                {
                    if (isGround)
                    {
                        Bomb();
                    }
                }
            }
        }
        public override void RecycleSelf()
        {
            base.RecycleSelf();
            InitEffect();
        }
        private void Bomb()
        {
            this.transform.parent = BattleMgr.S.bulletRoot;
            m_Nromal.SetActive(false);
            m_Bomb.SetActive(true);
            m_FirewordGroundBomb.SetActive(true);
            if (!isBomb)
            {
                isBomb = true;
                Collider[] cols = Physics.OverlapSphere(this.transform.position, 2.5f, LayerMask.GetMask("EnemyBody"));
                foreach (var item in cols)
                {
                    var enemy = item.GetComponent<RoleBase>();
                    if (enemy != null)
                    {
                        enemy.Hurted((this.transform.position - item.transform.position).normalized * m_Force * 0, m_Hurt, Define.AttackType.Firework);
                        enemy.ChangeVelocity((item.transform.position - this.transform.position).normalized * m_Force);// + new Vector3(0, 10, 0));
                    }
                }
                //RecycleSelf();
            }
        }

    }
}
