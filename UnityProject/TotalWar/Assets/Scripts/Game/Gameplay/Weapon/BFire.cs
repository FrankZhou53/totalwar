﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class BFire : BulletBase
    {
        [SerializeField] private Renderer m_Render;
        private int matChange = 1;
        private Vector3 vec;
        private Rigidbody m_Rig;
        protected override void InitEffect()
        {
            m_Rig = this.GetComponent<Rigidbody>();
        }
        public override void SetSpeed(Vector3 _speed)
        {
            base.SetSpeed(_speed);
            this.GetComponent<Rigidbody>().velocity = _speed;
            vec = _speed;
        }
        protected override bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            return _enemy.GetComponent<RoleBase>().Hurted(_force * m_Force, m_Hurt, Define.AttackType.Fire);
        }
        protected override void HurtGround()
        {

        }
        protected override void InitOther()
        {
        }
        protected override void FixedUpdateBullet()
        {
            //m_Rig.velocity = vec;
            // var Amount = m_Render.material.GetFloat("Vector1_FD63D5DE");
            // if (Amount >= 0.8f)
            // {
            //     matChange = -1;
            // }
            // else if (Amount <= 0.4f)
            // {
            //     matChange = 1;
            // }
            // m_Render.material.SetFloat("Vector1_FD63D5DE", Amount + (Time.deltaTime * matChange));
        }
        protected override void OnCollisionEnter(Collision other)
        {
            m_Rig.velocity = vec;
            isNew = false;
            if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))    //将检测结果的碰撞对象标签与player标签对比.判断是否相等
            {
                m_relDesTime = RandomHelper.Range(3.5f, 4.5f);
                var force = m_Rigidbody.velocity;
                //获取敌人
                GameObject body = other.gameObject;
                while (body.layer == LayerMask.NameToLayer("Enemy"))
                {
                    body = body.transform.parent.gameObject;
                }
                if (HurtEnemy(body, force))
                {
                    //Destroy(body.gameObject);
                    //EventSystem.S.Send(EventID.OnKillEnemy);
                }
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Ice"))
            {
                var enemy = other.transform.GetComponent<RoleBase>();
                enemy.Broken();
            }
        }
    }
}
