﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class WThrowMan : WeaponBase
    {
        [SerializeField] private GameObject m_ShowBullet;
        [SerializeField] private Animation m_Anim;
        protected override float ShowFireAction()
        {
            m_Anim.Stop();
            m_Anim.Play("attack02");
            m_FireBase.gameObject.SetActive(false);
            m_ShowBullet.SetActive(false);
            return base.ShowFireAction();
        }
        protected override void ShowReadyFireAction()
        {
            base.ShowReadyFireAction();
            m_FireBase.gameObject.SetActive(true);
            m_ShowBullet.SetActive(true);
        }
    }
}
