﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class BArrowFire : BulletBase
    {
        [SerializeField] private GameObject m_FireBomb;
        [SerializeField] private GameObject m_Fire;
        protected override void InitEffect()
        {
            m_FireBomb.SetActive(false);
        }
        protected override bool HurtEnemy(GameObject _enemy, Vector3 _force)
        {
            m_FireBomb.SetActive(true);
            _enemy.GetComponent<RoleBase>().AddEffect(m_Fire, this.transform.position, this.transform.parent);
            return _enemy.GetComponent<RoleBase>().Hurted(_force * m_Force, m_Hurt, Define.AttackType.Fire);
        }
        protected override void HurtGround()
        {
            m_FireBomb.SetActive(true);
        }
        public override void RecycleSelf()
        {
            base.RecycleSelf();
            m_FireBomb.SetActive(false);
        }

    }
}
