﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;
using UnityEngine.Rendering.Universal;

namespace GameWish.Game
{
    public class BattleMgr : TMonoSingleton<BattleMgr>
    {
        [SerializeField] private Transform m_PlayerRoot;
        [SerializeField] private Transform m_EnemyRoot;
        [SerializeField] private Transform m_EnemyPosRoot;
        [SerializeField] private Transform m_BulletRoot;
        [SerializeField] private Transform m_CameraRoot;
        [SerializeField] private List<Transform> m_LstCameraPos = new List<Transform>();
        [SerializeField] private List<WeaponBase> m_LstAllWeapon = new List<WeaponBase>();
        [SerializeField] private List<RoleBase> m_LstAllEnemy = new List<RoleBase>();
        [SerializeField] private List<RoleBase> m_LstAllBoss = new List<RoleBase>();
        //[SerializeField] private 
        public float maxMoveX = 7f;
        public Transform playerRoot { get { return m_PlayerRoot; } }
        public Transform enemyRoot { get { return m_EnemyRoot; } }
        public Transform bulletRoot { get { return m_BulletRoot; } }
        private float scaleTime = 0;
        private int timer = -1;
        private int timerGameEnd = -1;
        private List<RoleBase> m_LstEnemy = new List<RoleBase>();
        private int m_EnemyCount = 0;
        private Transform m_FireCameraPos;
        private WeaponBase m_Weapon;
        private List<WeaponBase> m_LstWeapon = new List<WeaponBase>();
        public bool isOver = false;
        private int countTime = 0;
        private int specialCount = 0;
        private int closedEnemyCount = 0;
        public void Init()
        {
            countTime = 0;
            closedEnemyCount = 0;
            isOver = false;
            m_CameraRoot = GameCamMgr.S.transform;
            ChangeCameraReady();
            Application.targetFrameRate = 144;
            //初始化武器
            specialCount = 0;
            InitWeapon();
            //初始化敌人
            CreateEnemy();

            m_FireCameraPos = m_LstCameraPos[1];

            EventSystem.S.Register(EventID.OnFireBullet, OnFireBullet);
            EventSystem.S.Register(EventID.OnKillEnemy, OnKillEnemy);
            EventSystem.S.Register(EventID.OnWeaponReady, OnWeaponReady);
            EventSystem.S.Register(EventID.OnEnemyEndingPos, OnEnemyEndingPos);
            EventSystem.S.Register(EventID.OnEnemyEndingPosDead, OnEnemyEndingPos);
            EventSystem.S.Register(EventID.OnGameOver, OnGameOver);


            UIMgr.S.OpenPanel(UIID.GamingPanel, LevelMgr.S.config.dicWeaponInfo.Count > 1);
        }
        public void ClearBattle()
        {
            EventSystem.S.UnRegister(EventID.OnFireBullet, OnFireBullet);
            EventSystem.S.UnRegister(EventID.OnKillEnemy, OnKillEnemy);
            EventSystem.S.UnRegister(EventID.OnWeaponReady, OnWeaponReady);
            EventSystem.S.UnRegister(EventID.OnEnemyEndingPos, OnEnemyEndingPos);
            EventSystem.S.UnRegister(EventID.OnEnemyEndingPosDead, OnEnemyEndingPos);
            EventSystem.S.UnRegister(EventID.OnGameOver, OnGameOver);
            m_LstEnemy.Clear();
            foreach (var item in m_LstWeapon)
            {
                item.ClearWeapon();
            }
            foreach (var item in m_LstEnemy)
            {
                Destroy(item.gameObject);
            }
            var bullet = m_BulletRoot.GetChildTrsList();
            foreach (var item in bullet)
            {
                Destroy(item.gameObject);
            }
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
            if (timerGameEnd != -1)
            {
                Timer.S.Cancel(timerGameEnd);
            }
            Destroy(this.gameObject);
        }
        public void InitWeapon(int _index = 0)
        {
            if (m_LstWeapon.Count == 0)
            {
                foreach (var item in LevelMgr.S.config.dicWeaponInfo)
                {
                    CreateWapon(m_LstAllWeapon[item.Key], item.Value);
                }
            }
            foreach (var item in m_LstWeapon)
            {
                item.gameObject.SetActive(false);
            }
            m_Weapon = m_LstWeapon[_index];
            m_Weapon.gameObject.SetActive(true);
            EventSystem.S.Send(EventID.OnWeaponReady);
        }
        void CreateWapon(WeaponBase _base, float _atk = -1)
        {
            var weapon = GameObject.Instantiate(_base, m_PlayerRoot).GetComponent<WeaponBase>();
            weapon.gameObject.SetActive(false);
            weapon.Init(_atk);
            m_LstWeapon.Add(weapon);
        }
        void CreateEnemy()
        {
            var enemySingle = m_LstAllEnemy[LevelMgr.S.config.enemyID].gameObject;
            var index = RandomHelper.Range(0, enemySingle.GetComponent<RoleBase>().renderCount);
            m_LstEnemy.Clear();
            foreach (var item in m_EnemyPosRoot.GetChildTrsList())
            {
                var enemy = GameObject.Instantiate(enemySingle).GetComponent<RoleBase>(); //GameObjectPoolMgr.S.Allocate("Enemy").GetComponent<RoleBase>();
                enemy.gameObject.SetActive(false);
                enemy.transform.position = item.position;
                enemy.transform.parent = BattleMgr.S.enemyRoot;
                enemy.Init(index);
                m_LstEnemy.Add(enemy);
            }
            m_EnemyCount = m_LstEnemy.Count;
            if (LevelMgr.S.config.isBoss)
            {
                var boss = GameObject.Instantiate(m_LstAllBoss[LevelMgr.S.config.enemyID].gameObject).GetComponent<RoleBase>();
                boss.gameObject.SetActive(false);
                boss.transform.position = m_EnemyPosRoot.GetChildTrsList()[0].position;
                boss.transform.parent = BattleMgr.S.enemyRoot;
                boss.Init(index);
                m_LstEnemy.Add(boss);
                m_EnemyCount += 1;
            }
        }
        // Update is called once per frame
        void Update()
        {
            if (scaleTime > 0)
            {
                scaleTime -= Time.deltaTime;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
        void ChangeCameraFire(float _percent)
        {
            // Log.e(_percent);
            m_CameraRoot.DOKill();
            m_CameraRoot.DOMove(m_FireCameraPos.position + (new Vector3(0, -0.8f, 8f) * _percent), 2.5f / m_Weapon.speed * 10f).SetUpdate(true);
            //m_CameraRoot.DOMove(m_FireCameraPos.position + new Vector3(0, 0, 5 * _percent), 2.5f / m_Weapon.speed * 10f).SetUpdate(true);
            m_CameraRoot.DORotateQuaternion(m_FireCameraPos.rotation, 2.5f / m_Weapon.speed * 10f).SetUpdate(true);
        }
        void ChangeCameraReady()
        {
            m_CameraRoot.DOKill();
            //m_Camera.DOMove(m_LstCameraPos[0].position, 0.2f);
            //m_Camera.DORotateQuaternion(m_LstCameraPos[0].rotation, 0.2f);
            m_CameraRoot.transform.position = m_LstCameraPos[0].position;
            m_CameraRoot.transform.rotation = m_LstCameraPos[0].rotation;
        }
        void OnWeaponReady(int key, params object[] args)
        {
            ChangeCameraReady();
        }
        void OnEnemyEndingPos(int key, params object[] args)
        {
            //m_FireCameraPos = m_LstCameraPos[2];
            if (key == (int)EventID.OnEnemyEndingPos)
            {
                closedEnemyCount += 1;
            }
            else
            {
                closedEnemyCount -= 1;
            }
        }
        void OnFireBullet(int key, params object[] args)
        {
            if (closedEnemyCount <= 0 && args.Length > 1)
            {
                float percent = (int)args[1] / 100f;
                scaleTime = 0f;
                Time.timeScale = 1f;
                ClearTimer();
                timer = Timer.S.Post2Really((int time) =>
                    {
                        scaleTime = 1f;
                        Time.timeScale = 0.4f;
                        timer = -1;
                        ChangeCameraFire(percent);
                    }, (float)args[0], 1);
            }
        }
        void OnKillEnemy(int key, params object[] args)
        {
            if (timer == -1)
            {
                ClearTimer();
                timer = Timer.S.Post2Really((int time) =>
                    {
                        scaleTime = 0f;
                        Time.timeScale = 1f;
                        timer = -1;
                    }, 1f, 1);
            }
            m_EnemyCount -= 1;
            if (m_EnemyCount <= 0 && !isOver)
            {
                specialCount += 1;
                //过关
                if (specialCount == LevelMgr.S.config.enemyCount)
                {
                    isOver = true;
                    PlayerInfoMgr.data.AddMaxLvl();
                    timerGameEnd = Timer.S.Post2Really((int time) =>
                        {
                            UIMgr.S.OpenPanel(UIID.GameEndingPanel, true);
                            timerGameEnd = -1;
                        }, 2f, 1);
                    //处理莫名活着的
                    foreach (var item in m_LstEnemy)
                    {
                        if (!item.isDead)
                        {
                            item.Hurted(Vector3.zero, item.MaxHp);
                        }
                    }
                }
                else//特殊关卡继续
                {
                    CreateEnemy();
                }
            }
        }
        void OnGameOver(int key, params object[] args)
        {
            countTime += 1;
            if (!isOver && countTime > 10)
            {
                isOver = true;
                UIMgr.S.OpenPanel(UIID.GameEndingPanel, false);
            }
        }
        void ClearTimer()
        {
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
        }
    }
}
