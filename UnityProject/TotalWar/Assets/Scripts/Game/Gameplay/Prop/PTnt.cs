﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class PTnt : PropBase
    {
        [SerializeField] private GameObject m_Self;
        [SerializeField] private GameObject m_BombEffect;
        public float m_Hurt = 100;
        public float m_BombRad = 10;
        public float m_Force = 10;
        protected void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Bullet"))
            {
                if (other.transform.GetComponent<BulletBase>().htype == Define.AttackType.Fire)
                {
                    Bomb();
                }
            }
        }
        void Bomb()
        {
            Collider[] cols = Physics.OverlapSphere(this.transform.position, m_BombRad, LayerMask.GetMask("EnemyBody"));
            foreach (var item in cols)
            {
                var enemy = item.GetComponent<RoleBase>();
                if (enemy != null)
                {
                    enemy.Hurted((this.transform.position - item.transform.position).normalized * m_Force * 0, m_Hurt, Define.AttackType.Firework);
                    enemy.ChangeVelocity((item.transform.position - this.transform.position).normalized * m_Force);// + new Vector3(0, 10, 0));
                }
            }
            m_BombEffect.SetActive(true);
            m_Self.SetActive(false);
            this.GetComponent<Collider>().isTrigger = true;
        }
    }
}
