using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using HedgehogTeam.EasyTouch;

namespace GameWish.Game
{
    public class SceneGame : ISceneBase
    {
        private SceneGameCtrller m_Ctrller;

        public SceneGame() { }

        public void OnSceneCtrllerInited(ISceneCtrller ctrller)
        {
            m_Ctrller = ctrller as SceneGameCtrller;
        }

        //场景加载
        public void OnSceneLoaded(params object[] args)
        {
            m_Ctrller.RegisterEvts();

            BattleMgr.S.Init();

            //UIMgr.S.OpenPanel(UIID.WorldUIPanel);
            UIMgr.S.ClosePanelAsUIID(UIID.LoadingPanel);
        }

        //场景重置
        public void OnSceneReset() { }

        public void OnSceneTick(float deltaTime)
        {
        }

        //关卡退出清理
        public void OnSceneClean()
        {
            m_Ctrller.UnRegisterEvts();

        }

        #region game_evts

        #endregion
    }

}