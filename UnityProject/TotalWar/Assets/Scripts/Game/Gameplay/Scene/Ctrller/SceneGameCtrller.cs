using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using HedgehogTeam.EasyTouch;
using Qarth;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameWish.Game
{
    public class SceneGameCtrller : MonoBehaviour, ISceneCtrller
    {
        [SerializeField] SceneEnum sceneType;
        // [SerializeField] private BallBrickModule m_ModuleBallBrick;
        // [SerializeField] private BalloonBattleModule m_ModuleBalloon;

        private List<IGameModule> m_LstModules = new List<IGameModule>();

        void Awake()
        {
            SceneLogicMgr.S.RegisterSceneCtrller(this, sceneType);
            GameCamMgr.S.AdjustCamSize();
            EasyTouch.AddCamera(GameCamMgr.S.gameplayCamera);

            // m_LstModules.Add(m_ModuleBallBrick);
            // m_LstModules.Add(m_ModuleBalloon);
        }

        void Start()
        {
            m_LstModules.ForEach(p => p.Init(this));
        }

        void Update()
        {
            m_LstModules.ForEach(p => p.Tick(Time.deltaTime));
        }

        void OnDestroy()
        {
            m_LstModules.ForEach(p => p.Clean());
        }

        public void RegisterEvts()
        {
            m_LstModules.ForEach(p => p.RegisterEvts());
        }

        public void UnRegisterEvts()
        {
            m_LstModules.ForEach(p => p.UnRegisterEvts());
        }
        public void NewLevel()
        {

        }
    }
}