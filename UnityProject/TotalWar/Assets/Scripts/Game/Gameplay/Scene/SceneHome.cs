using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class SceneHome : ISceneBase
    {
        private SceneHome m_Ctrller;

        public SceneHome()
        {

        }

        public void OnSceneCtrllerInited(ISceneCtrller ctrller)
        {
            m_Ctrller = ctrller as SceneHome;
        }

        //场景加载
        public void OnSceneLoaded(params object[] args)
        {
            //UIMgr.S.OpenPanel(UIID.MainPanel);
            EventSystem.S.Send(EventID.OnSceneLoaded);
            LevelMgr.S.StartNewGame();
        }

        //场景重置
        public void OnSceneReset()
        { }


        public void OnSceneTick(float deltaTime)
        { }


        //关卡退出清理
        public void OnSceneClean()
        { }
    }

}