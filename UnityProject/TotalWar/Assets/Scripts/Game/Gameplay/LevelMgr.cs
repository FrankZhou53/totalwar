﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class LevelMgr : TMonoSingleton<LevelMgr>
    {
        private TDLevelConfig m_Config;
        public TDLevelConfig config
        {
            get { return m_Config; }
        }
        public void StartNewGame()
        {
            m_Config = TDLevelConfigTable.GetData(PlayerInfoMgr.data.currentLevel);
            if (m_Config != null)
            {
                SceneLogicMgr.S.ChangeScene((SceneEnum)m_Config.sceneID, false);
            }
            else
            {
                FloatMessage.S.ShowMsg("Level Error!");
            }
        }
        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.F5))
            {
                BattleMgr.S.ClearBattle();
                PlayerInfoMgr.data.AddMaxLvl();
                StartNewGame();
            }
            else if (Input.GetKeyDown(KeyCode.F4))
            {
                BattleMgr.S.ClearBattle();
                PlayerInfoMgr.data.SubMaxLvl();
                StartNewGame();
            }
#endif
        }
    }
}
