﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class RoleBroken : MonoBehaviour
    {
        private float destoryTime;
        private float liveTime;
        private bool create = false;
        public void Init(bool _fly)
        {
            if (create)
            {
                return;
            }
            create = true;
            destoryTime = RandomHelper.Range(4, 6f);
            if (_fly)
            {
                this.transform.GetComponent<Rigidbody>().AddForce(new Vector3(RandomHelper.Range(-180f, 180f), RandomHelper.Range(-180, 180f), RandomHelper.Range(-180f, 180f)).normalized * 2000);
            }
        }
        void Update()
        {
            liveTime += Time.deltaTime;
            if (liveTime >= destoryTime)
            {
                Sequence quence = DOTween.Sequence();
                quence.Append(this.transform.DOScale(Vector3.zero, 0.3f));
                quence.AppendCallback(() =>
                {
                    Destroy(this.gameObject);
                });
            }
        }
        private void OnDestroy()
        {

        }
    }
}
