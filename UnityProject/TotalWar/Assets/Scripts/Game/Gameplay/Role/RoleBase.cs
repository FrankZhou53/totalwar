﻿using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Qarth;

namespace GameWish.Game
{
    public enum EnemyType
    {
        Normal,
        Boss,
    }
    public class RoleBase : MonoBehaviour
    {
        [SerializeField] public EnemyType type;
        [SerializeField] protected Animator m_Anim;
        [SerializeField] protected NavMeshAgent m_Agent;//寻路者
        [SerializeField] protected List<SkinnedMeshRenderer> m_LstRender = new List<SkinnedMeshRenderer>();
        [SerializeField] protected List<GameObject> m_LstBroken = new List<GameObject>();
        [SerializeField] protected List<Material> m_LstMat = new List<Material>();
        protected List<GameObject> m_LstEffect = new List<GameObject>();
        public int renderCount
        {
            get { return m_LstRender.Count; }
        }
        public List<Rigidbody> RagdollRigidbodys = new List<Rigidbody>();
        public List<Collider> RagdollColliders = new List<Collider>();
        protected SkinnedMeshRenderer m_Render;
        protected GameObject m_Broken;
        protected float m_Hp = 10;
        public bool isDead = false;
        protected bool isAttack = false;
        public float attackTime = 1f;
        protected float attackCd = 0f;
        protected bool isBroken = false;
        protected List<BulletBase> lstBullet = new List<BulletBase>();
        protected bool isClosed = false;
        public float hp
        {
            get { return m_Hp; }
            set
            {
                if (m_Hp <= 0)
                {
                    return;
                }
                m_Hp = value;
                if (m_Hp <= 0 && !isDead)
                {
                    isDead = true;
                    m_Agent.isStopped = true;
                    EventSystem.S.Send(EventID.OnKillEnemy);
                    if (isClosed)
                    {
                        EventSystem.S.Send(EventID.OnEnemyEndingPosDead);
                    }
                }
            }
        }
        public int MaxHp = 10;
        public float defense = 0f;

        protected int timer = -1;
        // Start is called before the first frame update
        public void Init(int _index)
        {
            m_Hp = MaxHp;
            foreach (var item in m_LstRender)
            {
                item.gameObject.SetActive(false);
            }
            m_Render = m_LstRender[_index];//RandomHelper.Range(0, m_LstRender.Count)];
            m_Render.gameObject.SetActive(true);
            m_Broken = m_LstBroken[_index];
            isDead = false;
            isBroken = false;
            isClosed = false;
            this.gameObject.SetActive(false);
            m_Agent.speed = RandomHelper.Range(0.6f, 1f);
            float delay = RandomHelper.Range(1, 3f);
            timer = Timer.S.Post2Really((int time) =>
            {
                this.gameObject.SetActive(true);
                timer = -1;
            }, delay, 1);
            InitRagdoll();
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            if (hp > 0)
            {
                Collider[] cols = Physics.OverlapSphere(this.transform.position, 1, LayerMask.GetMask("Weapon"));
                foreach (var item in cols)
                {
                    m_Agent.isStopped = true;
                    Attack();
                }
                m_Agent.SetDestination(BattleMgr.S.playerRoot.position);
                if (!isClosed && this.transform.position.x > BattleMgr.S.maxMoveX)
                {
                    isClosed = true;
                    EventSystem.S.Send(EventID.OnEnemyEndingPos);
                }
                attackCd += Time.deltaTime;
                if (isAttack && attackCd > attackTime)
                {
                    attackCd = 0;
                    EventSystem.S.Send(EventID.OnGameOver);
                }
            }
            else
            {
                if (RagdollRigidbodys[0].transform.position.y < -30)
                {
                    //GameObjectPoolMgr.S.Recycle(this.gameObject);
                    //this.gameObject.SetActive(false);
                    Destroy(this.gameObject);
                    return;
                }
                if (isBroken)
                {
                    var lst = m_Broken.transform.GetChildTrsList();
                    if (lst.Count == 0)
                    {
                        Destroy(this.gameObject);
                        return;
                    }
                }
                else
                {
                    m_Broken.transform.position = RagdollRigidbodys[0].position;
                    m_Broken.transform.rotation = RagdollRigidbodys[0].rotation;
                }
            }
        }
        void OnDestroy()
        {
            if (timer != -1)
            {
                Timer.S.Cancel(timer);
            }
        }
        void Attack()
        {
            isAttack = true;
            m_Anim.SetBool("isAttack", true);
        }
        public void AddForceToRole(Vector3 _force)
        {
            RagdollRigidbodys[0].AddForce(_force);
        }
        public void ChangeVelocity(Vector3 _force)
        {
            RagdollRigidbodys[0].velocity = _force;
        }
        public bool Hurted(Vector3 _force, float _hit = 0, Define.AttackType _type = Define.AttackType.Normal)
        {
            if (hp <= 0)
            {
                AddForceToRole(_force);
                DeadShow(_type);
                return false;
            }
            if (_type == Define.AttackType.Normal)
            {
                hp -= _hit * (1 - defense);
            }
            else
            {
                hp -= _hit;
            }
            if (hp <= 0)
            {
                m_Agent.isStopped = true;
                Destroy(m_Agent);
                EnableRagdoll();
                AddForceToRole(_force);
                HurtedShow(true, _type);
                return true;
            }
            HurtedShow(false, _type);
            return false;
        }
        protected virtual void HurtedShow(bool _isDead, Define.AttackType _type)
        {
            switch (_type)
            {
                case Define.AttackType.Ice:
                    {
                        if (_isDead)
                        {
                            ChangeAllMat(2);
                            DisableRagdoll(false);
                            this.GetComponent<Rigidbody>().isKinematic = false;
                            for (int i = 0; i < RagdollRigidbodys.Count; i++)
                            {
                                RagdollRigidbodys[i].gameObject.SetActive(false);
                            }
                            timer = Timer.S.Post2Really((int time) =>
                                {
                                    this.gameObject.layer = LayerMask.NameToLayer("Ice");
                                    timer = -1;
                                }, 1f, 1);
                        }
                        else
                        {
                            ChangeAllMat(1);
                            m_Agent.speed = m_Agent.speed * (float)hp / (float)MaxHp;
                        }
                    }
                    break;
                case Define.AttackType.Fire:
                    {
                        if (_isDead)
                        {
                            ChangeAllMat(3);
                            foreach (var item in m_LstEffect)
                            {
                                DestoryEffect(item);
                            }
                            m_LstEffect.Clear();
                        }
                        else
                        {
                            if (timer == -1)
                            {
                                timer = Timer.S.Post2Really((int time) =>
                                    {
                                        if (Hurted(Vector3.zero, 1, _type))
                                        {
                                            Timer.S.Cancel(timer);
                                            timer = -1;
                                        }
                                    }, 1f, -1);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

        }
        public void Broken(bool _isFly = true)
        {
            if (isBroken)
            {
                return;
            }
            hp = 0;
            isBroken = true;
            m_Render.gameObject.SetActive(false);
            m_Broken.SetActive(true);
            var lst = m_Broken.transform.GetChildTrsList();
            foreach (var item in lst)
            {
                item.GetComponent<RoleBroken>().Init(_isFly);
            }
            for (int i = 0; i < RagdollRigidbodys.Count; i++)
            {
                RagdollRigidbodys[i].gameObject.SetActive(false);
            }
        }
        protected virtual void ChangeAllMat(int _index)
        {
            Material[] mats = new Material[m_Render.materials.Length];
            for (var i = 0; i < m_Render.materials.Length; i++)
            {
                mats[i] = m_LstMat[_index];
            }
            m_Render.materials = mats;

            var lst = m_Broken.transform.GetChildTrsList();
            foreach (var item in lst)
            {
                var render = item.GetComponent<MeshRenderer>();
                Material[] matsB = new Material[render.materials.Length];
                for (var i = 0; i < render.materials.Length; i++)
                {
                    matsB[i] = m_LstMat[_index];
                }
                render.materials = matsB;
            }
        }
        public void AddEffect(GameObject _effect, Vector3 _pos, Transform _parent)
        {
            var effect = GameObject.Instantiate(_effect);
            effect.SetActive(true);
            effect.transform.position = _pos;
            effect.transform.parent = _parent;
            if (hp > 0)
            {
                m_LstEffect.Add(effect);
            }
            else
            {
                DestoryEffect(effect);
            }
            //Destroy(effect.gameObject, 8);
        }
        protected void DestoryEffect(GameObject _obj)
        {
            _obj.transform.localScale = Vector3.one;
            Destroy(_obj, RandomHelper.Range(2f, 4f));
        }
        void DeadShow(Define.AttackType _type)
        {
            switch (_type)
            {
                case Define.AttackType.Ice:
                    {

                    }
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 初始化，遍历角色身体上的所有ragdoll并存储 此时关闭布娃娃系统
        /// </summary>
        void InitRagdoll()
        {
            Rigidbody[] Rigidbodys = GetComponentsInChildren<Rigidbody>();
            for (int i = 0; i < Rigidbodys.Length; i++)
            {
                if (Rigidbodys[i] == GetComponent<Rigidbody>())
                {
                    //排除正常状态的Rigidbody
                    continue;
                }
                //添加Rigidbody和Collider到List
                RagdollRigidbodys.Add(Rigidbodys[i]);
                Rigidbodys[i].isKinematic = true;
                Collider RagdollCollider = Rigidbodys[i].gameObject.GetComponent<Collider>();
                //RagdollCollider.isTrigger = true;//这里需要布娃娃的碰撞器
                RagdollColliders.Add(RagdollCollider);
            }
        }
        /// <summary>
        /// 启动布娃娃系统
        /// </summary>
        public void EnableRagdoll()
        {
            //开启布娃娃状态的所有Rigidbody和Collider
            for (int i = 0; i < RagdollRigidbodys.Count; i++)
            {
                RagdollRigidbodys[i].isKinematic = false;
                RagdollColliders[i].isTrigger = false;
                //修改layer防止碰撞
                //RagdollColliders[i].gameObject.layer = LayerMask.NameToLayer("Default");
            }
            //关闭正常状态的Collider
            //GetComponent<Collider>().enabled = false;
            //下一帧关闭正常状态的动画系统
            StartCoroutine(SetAnimatorEnable(false));
        }
        /// <summary>
        /// 关闭布娃娃系统
        /// </summary>
        public void DisableRagdoll(bool _openAnim = true)
        {
            //关闭布娃娃状态的所有Rigidbody和Collider
            for (int i = 0; i < RagdollRigidbodys.Count; i++)
            {
                RagdollRigidbodys[i].isKinematic = true;
                RagdollColliders[i].isTrigger = true;
            }
            //开启正常状态的Collider
            GetComponent<Collider>().enabled = true;
            //下一帧开启正常状态的动画系统
            StartCoroutine(SetAnimatorEnable(_openAnim));
        }
        IEnumerator SetAnimatorEnable(bool Enable)
        {
            yield return new WaitForEndOfFrame();
            m_Anim.enabled = Enable;
        }
        ////
        public void AddBullet(BulletBase _bullet)
        {
            lstBullet.Add(_bullet);
        }
        protected void RemoveBullets()
        {
            foreach (var item in lstBullet)
            {
                item.RecycleSelf();
            }
        }
        public virtual void HurtOver(Define.AttackType _type, params object[] args)
        {

        }
    }
}
