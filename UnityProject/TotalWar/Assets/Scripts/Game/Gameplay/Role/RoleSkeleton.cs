﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class RoleSkeleton : RoleBase
    {
        private float flyTime = 0;
        private float flyDelay = 0;
        private bool isFly = false;
        protected override void Update()
        {
            base.Update();
            if (isFly)
            {
                flyDelay += Time.deltaTime;
                if (flyDelay >= flyTime)
                {
                    Broken(false);
                    DisableRagdoll(false);
                }
            }
        }
        protected override void HurtedShow(bool _isDead, Define.AttackType _type)
        {
            switch (_type)
            {
                case Define.AttackType.Ice:
                    {
                        if (_isDead)
                        {
                            ChangeAllMat(2);
                            RemoveBullets();
                            Broken();
                        }
                        else
                        {
                            ChangeAllMat(1);
                            m_Agent.speed = m_Agent.speed * (float)hp / (float)MaxHp;
                        }
                    }
                    break;
                case Define.AttackType.Fire:
                    {
                        if (_isDead)
                        {
                            ChangeAllMat(3);
                            RemoveBullets();
                            Broken();
                            foreach (var item in m_LstEffect)
                            {
                                DestoryEffect(item);
                            }
                            m_LstEffect.Clear();
                        }
                        else
                        {
                            if (timer == -1)
                            {
                                timer = Timer.S.Post2Really((int time) =>
                                    {
                                        if (Hurted(Vector3.zero, 1, _type))
                                        {
                                            Timer.S.Cancel(timer);
                                            timer = -1;
                                        }
                                    }, 1f, -1);
                            }
                        }
                    }
                    break;
                case Define.AttackType.Balloon:
                    {
                        this.gameObject.layer = LayerMask.NameToLayer("Default");
                    }
                    break;
                default:
                    if (_isDead)
                    {
                        Broken();
                    }
                    break;
            }

        }
        public override void HurtOver(Define.AttackType _type, params object[] args)
        {
            switch (_type)
            {
                case Define.AttackType.Balloon:
                    {
                        isFly = true;
                        float delay = Mathf.Sqrt(RagdollRigidbodys[0].position.y * 3.6f / (-Physics.gravity.y));
                        if (delay > flyTime)
                        {
                            flyTime = delay;
                            flyDelay = 0;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
