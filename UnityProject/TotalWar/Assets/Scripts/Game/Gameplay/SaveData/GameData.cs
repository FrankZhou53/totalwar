﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class GameData : IDataClass
    {
        public bool notNewbie;
        private BigInteger m_GameToken;
        public double cash;
        public string gameToken = "0";
        public EInt cardTicket;

        public string lastPlayTimeString = "0";
        public bool vibrateFlag = true;
        public EInt currentLevel = 0;

        public int tokenPhase;
        public int cashPhase;

        public List<int> collecttableIds = new List<int>();
        private List<int> m_TmpColloctions = new List<int>();

        public GameData()
        {
            SetDirtyRecorder(PlayerInfoMgr.dataDirtyRecorder);
        }

        public override void InitWithEmptyData()
        {
            Log.w("InitWithEmptyData");

            m_GameToken = new BigInteger(gameToken);
            cash = 0;
        }

        public override void OnDataLoadFinish()
        {
            m_GameToken = new BigInteger(gameToken);


            SetDataDirty();
        }

        public void ResetData()
        {
            InitWithEmptyData();
        }

        public void ResetDailyParams()
        {
            if (CustomExtensions.CheckIsNewDay(Define.SIGN_DAY_KEY) > 0)
            {
                SetDataDirty();
            }
        }

        public void SetLastPlayTime(string time)
        {
            if (long.Parse(time) > long.Parse(lastPlayTimeString))
            {
                lastPlayTimeString = time;
                SetDataDirty();
            }
        }

        public void SetVibrateState(bool state)
        {
            vibrateFlag = state;
            SetDataDirty();
        }

        #region game_funcs
        public void CloseNewbie()
        {
            notNewbie = true;
            SetDataDirty();
        }

        public void AddMaxLvl()
        {
            currentLevel += 1;
            if (currentLevel >= TDLevelConfigTable.count)
            {
                currentLevel = 0;
            }
            SetDataDirty();
        }
        public void SubMaxLvl()
        {
            currentLevel -= 1;
            if (currentLevel <= 0)
            {
                currentLevel = TDLevelConfigTable.count - 1;
            }
            SetDataDirty();
        }

        public int GetCurStage()
        {
            return currentLevel;// % Define.BALLOON_STAGE_LVL;
        }

        public void AddTokenCount(BigInteger count)
        {
            m_GameToken += count;
            gameToken = m_GameToken.ToString();
            SetDataDirty();

            EventSystem.S.Send(EventID.OnPropertyAdd);
        }

        public void AddCashCount(float count)
        {
            cash += count;
            cash = Mathf.Clamp((float)cash, 0, Define.SAFETY_CASH_COUNT);
            SetDataDirty();

            EventSystem.S.Send(EventID.OnPropertyAdd);
        }

        public void AddCardTicket(int count)
        {
            cardTicket += count;
            cardTicket = Mathf.Clamp(cardTicket, 0, Define.SAFETY_CARD_COUNT);
            SetDataDirty();

            EventSystem.S.Send(EventID.OnPropertyAdd);
        }

        public BigInteger GetTokenCount()
        {
            return m_GameToken;
        }
        #endregion
    }
}