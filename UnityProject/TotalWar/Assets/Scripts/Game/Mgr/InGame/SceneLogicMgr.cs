﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Qarth;

namespace GameWish.Game
{
    public enum SceneEnum
    {
        Home,
        Gaming1,
        Gaming2,
        Gaming3,
        Gaming4,
    }

    public class SceneLogicMgr : TMonoSingleton<SceneLogicMgr>
    {
        private SceneEnum m_CurSceneEnum;
        public SceneEnum CurSceneEnum
        {
            get { return m_CurSceneEnum; }
        }
        private SceneEnum m_LastSceneEnum;
        public SceneEnum LastSceneEnum
        {
            get { return m_LastSceneEnum; }
        }

        private ISceneBase m_CurScene;
        public ISceneBase CurScene
        {
            get { return m_CurScene; }
        }

        private Dictionary<int, ISceneBase> m_DicScenes = new Dictionary<int, ISceneBase>();
        private bool m_GameStarted;
        private bool m_Loading;


        public void Init()
        {
            m_DicScenes.Add((int)SceneEnum.Home, new SceneHome());
            m_DicScenes.Add((int)SceneEnum.Gaming1, new SceneGame());
            m_DicScenes.Add((int)SceneEnum.Gaming2, new SceneGame());
            m_DicScenes.Add((int)SceneEnum.Gaming3, new SceneGame());
            m_DicScenes.Add((int)SceneEnum.Gaming4, new SceneGame());

            m_GameStarted = false;
            m_CurSceneEnum = SceneEnum.Home;
            DoSceneLogic(m_CurSceneEnum);
        }

        void Update()
        {
            TickSceneLogic(Time.deltaTime);
        }

        //切换场景方法供外部调用
        public void ChangeScene(SceneEnum type, bool additive, params object[] args)
        {
            if (m_Loading)
                return;
            m_Loading = true;
            UIMgr.S.OpenTopPanel(UIID.LoadingPanel, (panel) =>
            {
                StartCoroutine(LoadScene(type, false, args));
            }, null);
        }

        public void RegisterSceneCtrller(ISceneCtrller ctrller, SceneEnum type)
        {
            if (m_DicScenes.ContainsKey((int)type))
            {
                m_DicScenes[(int)type].OnSceneCtrllerInited(ctrller);
            }
        }

        //加载场景
        private IEnumerator LoadScene(SceneEnum type, bool additive, params object[] args)
        {
            yield return new WaitForSeconds(0.5f);
            CleanCurrentScene();
            int loadingProgress = 0;
            var sceneName = type.ToString();

            AsyncOperation oprAsync = additive ?
                SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive)
                : SceneManager.LoadSceneAsync(sceneName);
            oprAsync.allowSceneActivation = false;

            while (!oprAsync.isDone)//done这个变量在allowSceneActivation为true以后自然会设置
            {
                int progressLimit = 0;
                if (oprAsync.progress < 0.85f)//unity的机制是卡在0.9,防止浮点数不准确,写0.85
                    progressLimit = (int)(oprAsync.progress * 100);
                else
                    progressLimit = 100;
                if (loadingProgress <= progressLimit)
                {
                    loadingProgress += 2;//这个阶段速度可以控制
                }
                else if (progressLimit == 100)
                {
                    oprAsync.allowSceneActivation = true;
                }
                yield return new WaitForEndOfFrame();
            }

            //!!important
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
            m_Loading = false;
            DoSceneLogic(type, args);

            oprAsync = null;
        }
        private IEnumerator UnloadAdditiveLevelScene(SceneEnum nextType, params object[] args)
        {
            CleanCurrentScene();
            var oprAsync = SceneManager.UnloadSceneAsync(m_LastSceneEnum.ToString());
            while (!oprAsync.isDone)
            {
                yield return new WaitForEndOfFrame();
            }
            DoSceneLogic(nextType, args);
        }

        //加载关卡逻辑控制
        private void DoSceneLogic(SceneEnum type, params object[] args)
        {
            m_LastSceneEnum = m_CurSceneEnum;
            m_CurSceneEnum = type;

            if (m_DicScenes.ContainsKey((int)m_CurSceneEnum))
            {
                m_CurScene = m_DicScenes[(int)m_CurSceneEnum];
                m_CurScene.OnSceneLoaded(args);
            }
        }

        private void CleanCurrentScene()
        {
            if (m_CurScene != null)
                m_CurScene.OnSceneClean();
        }

        private void TickSceneLogic(float deltaTime)
        {
            if (m_CurScene != null)
                m_CurScene.OnSceneTick(deltaTime);
        }
    }
}