﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class CommonResModule : AbstractModule
    {
        protected override void OnComAwake()
        {
            HolderCommondAudio();
            ResHolder.S.loader.LoadSync();
        }

        private void HolderCommondAudio()
        {
            ResHolder.S.AddRes(TDConstTable.QueryString(ConstType.SOUND_DEFAULT_BUTTON));

            // ResHolder.S.AddRes("audio_cashget");
            // ResHolder.S.AddRes("audio_coinsget1");
            // ResHolder.S.AddRes("audio_coinsget2");
            // ResHolder.S.AddRes("audio_coinsget3");
            // ResHolder.S.AddRes("audio_shot");
        }

    }
}
