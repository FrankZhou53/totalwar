﻿using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class ButtonDragCommand : AbstractGuideCommand
    {
        private IUINodeFinder m_Finder;
        private Transform m_TargetButton;
        private bool m_HasDown = false;
        private static List<RaycastResult> m_Result = new List<RaycastResult>();

        public override void SetParam(object[] param)
        {
            m_Finder = param[0] as IUINodeFinder;
        }

        protected override void OnStart()
        {
            m_TargetButton = m_Finder.FindNode(false);

            if (m_TargetButton == null)
            {
                return;
            }

            UIMgr.S.topPanelHideMask = PanelHideMask.UnInteractive;
            AppLoopMgr.S.onUpdate += Update;
        }

        protected override void OnFinish(bool forceClean)
        {
            UIMgr.S.topPanelHideMask = PanelHideMask.None;
            AppLoopMgr.S.onUpdate -= Update;
        }

        private void OnClickUpOnTarget()
        {
            ExecuteEvents.Execute<IEndDragHandler>(m_TargetButton.gameObject, new PointerEventData(UnityEngine.EventSystems.EventSystem.current), ExecuteEvents.endDragHandler);
            //FinishStep();
        }

        private void OnClickDownOnTarget()
        {
            ExecuteEvents.Execute<IBeginDragHandler>(m_TargetButton.gameObject, new PointerEventData(UnityEngine.EventSystems.EventSystem.current), ExecuteEvents.beginDragHandler);
        }

        private void OnDragOnTarget()
        {
            ExecuteEvents.Execute<IDragHandler>(m_TargetButton.gameObject, new PointerEventData(UnityEngine.EventSystems.EventSystem.current), ExecuteEvents.dragHandler);
        }

        private void Update()
        {
            if (m_TargetButton == null)
            {
                AppLoopMgr.S.onUpdate -= Update;
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                m_HasDown = true;
                OnClickDownOnTarget();
            }

            if (m_HasDown)
            {
                OnDragOnTarget();
            }
            if (!m_HasDown)
            {
                return;
            }

            if (Input.GetMouseButtonUp(0))
            {
                m_HasDown = false;
                CheckIsTouchInTarget();
                OnClickUpOnTarget();
            }
        }

        private bool CheckIsTouchInTarget()
        {
            PointerEventData pd = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
            pd.position = Input.mousePosition;

            var graphicRaycasr = m_TargetButton.GetComponentInParent<GraphicRaycaster>();

            if (graphicRaycasr == null)
            {
                return false;
            }

            graphicRaycasr.Raycast(pd, m_Result);

            if (m_Result.Count == 0)
            {
                return false;
            }

            for (int i = 0; i < m_Result.Count; i++)
            {
                // if (m_Result[i].gameObject.GetComponent<GridItem>() != null)
                // {
                //     pd.pointerEnter = m_Result[i].gameObject;
                //     m_Result.Clear();
                //     return true;
                // }
            }

            //if (IsHitWhiteObject(m_Result))
            //{
            //    m_Result.Clear();
            //    return true;
            //}

            m_Result.Clear();
            return false;
        }

        private bool IsHitWhiteObject(List<RaycastResult> result)
        {
            if (result == null || result.Count == 0)
            {
                return false;
            }

            for (int i = result.Count - 1; i >= 0; --i)
            {
                GameObject go = result[i].gameObject;
                if (go != null)
                {
                    if (IsHitWhiteObject(go.transform))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsHitWhiteObject(Transform tr)
        {
            if (tr.IsChildOf(m_TargetButton.transform))
            {
                return true;
            }

            return false;
        }
    }
}

