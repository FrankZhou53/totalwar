﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class GuideModule : AbstractModule
    {
        public void StartGuide()
        {
            if (!AppConfig.S.isGuideActive)
            {
                return;
            }
            InitCustomTrigger();
            InitCustomCommand();

            GuideMgr.S.StartGuideTrack();
        }

        protected override void OnComAwake()
        {

        }

        protected void InitCustomTrigger()
        {
            // GuideMgr.S.RegisterGuideTrigger(typeof(ChanceAddTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(UINodeInvisibleTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(MoveEnableTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(ExpMaxTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(TaskCompleteTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(GuidePropertyCompleteTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(EmptySlotTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(UICloseTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(LevelBeginTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(EnterSceneTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(PassStageTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(BoosterTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(SkillReadyTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(OreReadyTrigger));
            // GuideMgr.S.RegisterGuideTrigger(typeof(EnterChapterTrigger));

        }

        protected void InitCustomCommand()
        {
            // GuideMgr.S.RegisterGuideCommand(typeof(WaitCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(HighlightUIsCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(ButtonDragCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(CharaWordsCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(FloatWordsCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(OpenTopPanelCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(GuideEliminateCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(RefreshPauseCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(MyGuideWordsCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(GuideLevelCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(MyButtonHackCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(MyButtonClickHackCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(FinishGuideStageCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(GuideBoosterCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(GuideSkillCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(GuideChapterCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(FinishStepCommand));

            // GuideMgr.S.RegisterGuideCommand(typeof(CharaWordsCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(ShowGuideLineCommand));
        }
    }
}
