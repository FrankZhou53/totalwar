﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class TaskCompleteTrigger : ITrigger
    {
        protected Action<bool, ITrigger> m_Listener;

        public bool isReady
        {
            get
            {
                return false; //TaskMgr.S.GetCurrentTask().CheckComplete() >= 1; }
            }
        }
        public void SetParam(object[] param)
        {

        }

        public void Start(Action<bool, ITrigger> l)
        {
            m_Listener = l;
            EventSystem.S.Register(EventID.TaskStateChanged, OnEventListerner);
            OnEventListerner(0);
        }

        public void Stop()
        {
            m_Listener = null;
            EventSystem.S.UnRegister(EventID.TaskStateChanged, OnEventListerner);
        }

        private void OnEventListerner(int key, params object[] args)
        {
            if (m_Listener == null)
            {
                return;
            }

            if (isReady)
            {
                m_Listener(true, this);
                m_Listener = null;
            }
            else
            {
                m_Listener(false, this);
            }

        }
    }
}

