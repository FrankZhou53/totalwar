﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
	public class CheckBtnEnableTrigger : ITrigger
	{
	    private Action<bool, ITrigger> m_Listener;
	    private bool m_IsBtnEnable;

        public bool isReady
	    {
            get { return m_IsBtnEnable; }
	    }
        public void SetParam(object[] param)
        {
        }

        public void Start(Action<bool, ITrigger> l)
        {
            m_Listener = l;
            m_IsBtnEnable = false;

            EventSystem.S.Register(EventID.UnLockPicBtn, OnTriggerStart);
            
        }

        public void Stop()
        {
            EventSystem.S.UnRegister(EventID.UnLockPicBtn, OnTriggerStart);
            m_Listener = null;
        }

	    void OnTriggerStart(int key,params object[] param)
	    {
	        if (m_Listener == null)
	        {
	            return;
	        }
	        m_IsBtnEnable = true;

	        m_Listener(true, this);
	        m_Listener = null;
        }
    }
}

