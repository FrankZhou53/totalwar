﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum UIID
    {
        LogoPanel = 0,
        //测试工具界面
        ToolsPanel,
        SplashPanel,
        GuideWordsPanel,
        LoadingPanel,
        WorldUIPanel,
        GamingPanel,
        GameEndingPanel,
        MyGuideWordsPanel,
        MainPanel,


    }
}
