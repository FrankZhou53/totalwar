﻿using System.Collections;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class UIDataModule : AbstractModule
    {
        public static void RegisterStaticPanel()
        {
            InitUIPath();
            UIDataTable.SetABMode(false);
            UIDataTable.AddPanelData(UIID.LogoPanel, null, "LogoPanel/LogoPanel");
            UIDataTable.AddPanelData(UIID.SplashPanel, null, "LogoPanel/SplashPanel");
        }

        protected override void OnComAwake()
        {
            InitUIPath();
            RegisterAllPanel();
        }

        private static void InitUIPath()
        {
            PanelData.PREFIX_PATH = "Resources/UI/Panels/{0}";
            PageData.PREFIX_PATH = "Resources/UI/Panels/{0}";
        }

        private void RegisterAllPanel()
        {
            UIDataTable.SetABMode(true);

            UIDataTable.AddPanelData(EngineUI.FloatMessagePanel, null, "Common/FloatMessagePanel", true, 1);
            UIDataTable.AddPanelData(EngineUI.MsgBoxPanel, null, "Common/MsgBoxPanel", true, 1);
            UIDataTable.AddPanelData(EngineUI.HighlightMaskPanel, null, "Guide/HighlightMaskPanel", true, 0);
            UIDataTable.AddPanelData(EngineUI.GuideHandPanel, null, "Guide/GuideHandPanel", true, 0);
            UIDataTable.AddPanelData(EngineUI.MaskPanel, null, "Common/MaskPanel", true, 1);
            UIDataTable.AddPanelData(EngineUI.ColorFadeTransition, null, "Common/ColorFadeTransition", true, 1);
            // UIDataTable.AddPanelData(SDKUI.AdDisplayer, null, "Common/AdDisplayer", false, 1);
            // UIDataTable.AddPanelData(SDKUI.OfficialVersionAdPanel, null, "OfficialVersionAdPanel");

            UIDataTable.AddPanelData(UIID.GuideWordsPanel, null, "GamePanel/GuidePanel/GuideWordsPanel");
            UIDataTable.AddPanelData(UIID.WorldUIPanel, null, "GamePanel/WorldUIPanel/WorldUIPanel", true);
            UIDataTable.AddPanelData(EngineUI.RatePanel, null, "GamePanel/RatePanel", true);
            UIDataTable.AddPanelData(UIID.LoadingPanel, null, "GamePanel/LoadingPanel/LoadingPanel", true, 1);

            UIDataTable.AddPanelData(UIID.GamingPanel, null, "GamePanel/GamingPanel/GamingPanel", true);
            UIDataTable.AddPanelData(UIID.GameEndingPanel, null, "GamePanel/GamingPanel/GameEndingPanel", true);
        }

    }
}